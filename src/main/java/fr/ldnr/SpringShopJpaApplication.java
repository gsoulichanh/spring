package fr.ldnr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import fr.ldnr.dao.ArticleRepository;
import fr.ldnr.dao.CategoryRepository;
import fr.ldnr.entities.Article;
import fr.ldnr.entities.Category;

@SpringBootApplication
public class SpringShopJpaApplication implements CommandLineRunner{
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private ArticleRepository articleRepository;

	public static void main(String[] args) {
		SpringApplication.run(SpringShopJpaApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception{
		//categoryRepository.save(new Category("Smartphone"));
		//articleRepository.save(new Article("S9","Samsung",250));
		
		/*Category smartphone = categoryRepository.save(new Category("Smartphone"));
		Category tablet = categoryRepository.save(new Category("Tablet"));
		Category pc = categoryRepository.save(new Category("PC"));
		
		articleRepository.save(new Article("S9","Samsung",350,smartphone));
		articleRepository.save(new Article("S10","Samsung",500,smartphone));
		articleRepository.save(new Article("MI10","Xiaomi",100, smartphone));
		
		articleRepository.save(new Article("GalaxyTab","Samsung",450, tablet));
		articleRepository.save(new Article("Ipad","Apple",350, tablet));
		
		articleRepository.save(new Article("R510","Asus",600, pc));
		articleRepository.save(new Article("O870","Omen",1200, pc));*/
		
		
		for(Article article : articleRepository.findByBrand("Samsung")) {
			System.out.println(article);
		}
		
		for(Article article : articleRepository.findByBrandAndPrice("Samsung",250)) {
			System.out.println(article);
		}
		
		for(Article article : articleRepository.findByBrandAndPriceGreaterThan("Samsung",300)) {
			System.out.println(article);
		}
		
		for(Article article : articleRepository.searchArticles("sung",200)) {
			System.out.println(article);
		}
		
		for(Article article : articleRepository.findByCategoryId((long) 5)) {
			System.out.println(article);
		}
		
		for(Article article : articleRepository.findByCategoryId((long) 6)) {
			System.out.println(article);
		}
		
		for(Article article : articleRepository.findByCategoryId((long) 7)) {
			System.out.println(article);
		}
	}
}
